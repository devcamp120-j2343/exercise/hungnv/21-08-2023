import com.devcamp.bookauthor.models.Author;
import com.devcamp.bookauthor.models.Book;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Author author1 = new Author("Nguyen Viet Hung","HungNV",'m');
        System.out.println("Author 1:");
        System.out.println(author1);

        Author author2 = new Author("Nguyen Thi B", "BNT", 'f');
        System.out.println("Author 2:");
        System.out.println(author2);

        Book book1 = new Book("Harry Poter",author1,21.0000,6);
        System.out.println("Book 1:");
        System.out.println(book1);

        Book book2 = new Book("Sword Art Online", author2, 50.000, 10);
        System.out.println("Book 2:");
        System.out.println(book2);
    }
}
