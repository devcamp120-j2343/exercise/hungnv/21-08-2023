import javafx.scene.shape.Circle;
import models.Shape;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println("Shape 1:");
        System.out.println(shape1.toString());
        System.out.println("Shape 2:");
        System.out.println(shape2.toString());

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(2.0, 3.0, 3.0);
        System.out.println(circle3.toString()); 
    }
}
