package models;

public class Circle extends Shape {
    double radius = 1.0;

    public void Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle( double radius, boolean filled,String color) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double getPerimeter() {
        return radius * 2 * Math.PI;
    }

    @Override
    public String toString() {
        return "Circle [Shape [color=" + color + ", filled=" + filled + "]" + "]" + ", radius=" + radius + "]";
    }

}
