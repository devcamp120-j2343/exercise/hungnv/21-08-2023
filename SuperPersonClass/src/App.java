import com.devcamp.personstudentstaff.models.Person;
import com.devcamp.personstudentstaff.models.Staff;
import com.devcamp.personstudentstaff.models.Student;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Person person1 = new Person("Nguyen Viet Hung", "Ha Dong");
        System.out.println("Person 1:");
        System.out.println(person1.toString());

        Person person2 = new Person("Nguyen Van A", "Sai gon");
        System.out.println("Person 2:");
        System.out.println(person2.toString());

        Student student1 = new Student(person1.getName(), person1.getAddress(), "Java", 2023, 2);
        System.out.println("Student 1:");
        System.out.println(student1.toString());

        Student student2 = new Student(person2.getName(), person2.getAddress(),"C++",2022,3);
        System.out.println("Person 2:");
        System.out.println(student2.toString());

        Staff staff1 = new Staff(person1.getName(),person1.getAddress(),"FPT", 20000);
        System.out.println("Staff 1:");
        System.out.println(staff1.toString());

        Staff staff2 = new Staff(person2.getName(),person2.getAddress(),"DH", 50000);
        System.out.println("Staff 2:");
        System.out.println(staff2.toString());
    }
}
