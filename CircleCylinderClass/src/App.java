import com.devcamp.circlecylineder.models.Circle;
import com.devcamp.circlecylineder.models.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Circle circle1 = new Circle();
        System.out.println("Circle 1:");
        System.out.println(circle1);

        Circle circle2 = new Circle(5);
        System.out.println("Circle 2:");
        System.out.println(circle2);

        Circle circle3 = new Circle(7, "Green");
        System.out.println("Circle 3:");
        System.out.println(circle3);

        System.out.println("Dien tich: ");
        System.out.println(circle1.getArea());

        Cylinder cylinder1 = new Cylinder();
        System.out.println("Cylinder 1:");
        System.out.println(cylinder1);

        Cylinder cylinder2 = new Cylinder(2.5);
        System.out.println("Cylinder 2:");
        System.out.println(cylinder2);

        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        System.out.println("Cylinder 3:");
        System.out.println(cylinder3);

        Cylinder cylinder4 = new Cylinder(7, "Blue", 1.5);
        System.out.println("Cylinder 4:");
        System.out.println(cylinder4);

        System.out.println("Dien tich Cylinder 4:");
        System.out.println(cylinder4.getArea());
    }
}
