import java.util.Date;

import com.devcamp.customervisit.models.Customer;
import com.devcamp.customervisit.models.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Customer customer1 = new Customer("Hung", true, "Thuong gia");
        Customer customer2 = new Customer("Nguyen", false, "Binh dan");
        System.out.println("Customer 1:");
        System.out.println(customer1.toString());
        System.out.println("Customer 2:");
        System.out.println(customer2.toString());

        Date date = new Date();
        Visit visit1 = new Visit("Nha trang",date);
        System.out.println("Visit 1:");
        System.out.println(visit1.toString());
        Visit visit2 = new Visit("Da Lat",date);
        System.out.println("Visit 2:");
        System.out.println(visit2.toString());
    }
}
