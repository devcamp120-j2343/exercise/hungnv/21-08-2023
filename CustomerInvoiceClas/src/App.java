import com.devcamp.customerinvoice.models.Customer;
import com.devcamp.customerinvoice.models.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Customer customer1 = new Customer(1, "Nguyen Viet Hung", 3);
        System.out.println("Customer 1:");
        System.out.println(customer1.toString());

        Customer customer2 = new Customer(2, "Nguyen Van A", 10);
        System.out.println("Customer 2:");
        System.out.println(customer2);

        Invoice invoice1 = new Invoice(1, customer1, 100000);
        System.out.println("Invoice 1:");
        System.out.println(invoice1.toString());

        Invoice invoice2 = new Invoice(2, customer2, 200000);
        System.out.println("Invoice 2:");
        System.out.println(invoice2.toString());

        System.out.println("Amount after discount: ");
        System.out.println(invoice2.getAmountAfterDiscount());
    }
}
